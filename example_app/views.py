from django.http import HttpRequest, HttpResponse
from .models import VisitCount


def show_visitors(request: HttpRequest) -> HttpResponse:
    """Display a message with the current visit count for the source IP."""
    proxy_trace = request.META.get("HTTP_X_FORWARDED_FOR")
    if proxy_trace is not None:
        source = proxy_trace.split(",", 1)[0]
    else:
        source = request.META.get("REMOTE_ADDR")
    visits = VisitCount.inc(source=source)
    formatted_source = (
        f"<code>{source}</code>" if source is not None else "unknown source"
    )
    html = (
        f"<html><body>Welcome visitor #{visits} from {formatted_source}!</body></html>"
    )
    return HttpResponse(html)
