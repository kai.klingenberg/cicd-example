# Generated by Django 4.0.4 on 2022-04-25 17:49

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='VisitCount',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('source', models.GenericIPAddressField(blank=True, null=True, unique=True)),
                ('count', models.PositiveBigIntegerField(default=1)),
            ],
        ),
    ]
