from typing import Optional
from django.db import models, transaction


class VisitCount(models.Model):
    """A record of visit counts, mapped to the source IP."""

    source = models.GenericIPAddressField(null=True, blank=True, unique=True)
    count = models.PositiveBigIntegerField(default=1)

    def __str__(self):
        return (
            f"Visits from {self.source}: {self.count}"
            if self.source is not None
            else f"Visits from unknown sources: {self.count}"
        )

    @classmethod
    @transaction.atomic
    def inc(cls, *, source: Optional[str]) -> int:
        try:
            record = cls.objects.get(source=source)
            record.count = models.F("count") + 1
            record.save()
            record.refresh_from_db()
            return record.count
        except cls.DoesNotExist:
            cls.objects.create(source=source)
            return 1
