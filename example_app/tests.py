import re
from typing import Optional
from django.test import TestCase


class VisitCountTestCase(TestCase):
    def extract_visit_count(self, response) -> Optional[int]:
        """Utility function that extracts a visit count from a response."""
        content = response.content.decode()
        count_match = re.search(r"#(\d+)", content)
        return int(count_match.group(1)) if count_match is not None else None

    def test_count_consistency(self):
        """The reported visit count is increasing."""
        # Arrange
        # Act
        first_visit_response = self.client.get("/")
        first_count = self.extract_visit_count(first_visit_response)
        second_visit_response = self.client.get("/")
        second_count = self.extract_visit_count(second_visit_response)
        # Assert
        self.assertEqual(first_visit_response.status_code, 200)
        self.assertEqual(second_visit_response.status_code, 200)
        self.assertIsNotNone(first_count)
        self.assertIsNotNone(second_count)
        self.assertGreater(second_count, first_count)
