from django.contrib import admin
from .models import VisitCount


@admin.register(VisitCount)
class VisitCountAdmin(admin.ModelAdmin):
    pass
