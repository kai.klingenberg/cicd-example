FROM python:3.8.5-slim

RUN set -eux ; \
    apt-get update ; \
    apt-get install -y --no-install-recommends \
        netcat=1.10-41.1 \
        libpq-dev=11.14-0+deb10u1 \
        build-essential=12.6 ; \
    rm -rf /var/lib/apt/lists/*

WORKDIR /src
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 8000
CMD [ \
    "gunicorn", \
    "cicd_example.asgi:application", \
    "-k", \
    "uvicorn.workers.UvicornWorker", \
    "-b", \
    "0.0.0.0:8000" \
]
