# Ejemplo CI/CD

[![pipeline status](https://gitlab.com/kai.klingenberg/cicd-example/badges/develop/pipeline.svg)](https://gitlab.com/kai.klingenberg/cicd-example/-/commits/develop)
[![coverage report](https://gitlab.com/kai.klingenberg/cicd-example/badges/develop/coverage.svg)](https://gitlab.com/kai.klingenberg/cicd-example/-/commits/develop)

Este repositorio contiene un ejemplo de configuración de _pipeline_ de
integración y despliegue continuos, específicamente aplicado a un
proyecto Django. El concepto _pipeline_ es explicado en detalle en el
archivo de definición.

El ejemplo abarca los siguientes aspectos:

- La configuración de un _pipeline_ de acuerdo a un flujo de trabajo
  común git, que consiste de dos ramas principales: una para hacer
  converger desarrollos puntuales (_develop_), y otra para desplegar
  versiones estables (_main_).
- La configuración y uso de herramientas de análisis estático de
  código. Estas herramientas reducen el esfuerzo necesario para
  mantener la **calidad** y **homogeneidad** del código fuente.
- La ejecución automática de tests al momento de recibir commits en el
  repositorio central. Su ejecución automatizada permite aprovecharlos
  plenamente como instrumento de detección de regresiones.
- El despliegue automático de aplicaciones, con mínima o nula
  participación humana. La automatización del depliegue de
  aplicaciones reduce el riesgo de cometer errores y forma parte del
  paradigma "despliegue como código" (_deployment as code_), que a su
  vez es parte del paradigma más amplio "todo como código"
  (_everything as code_). Ambos son paradigmas aceptados en la
  industria TI.

Además, este ejemplo hace uso de algunas pequeñas comodidades que
ofrece Gitlab para decorar un repositorio, tales como reportes de
cobertura de tests, o _badges_ de estado.

## Estructura -- Qué y en qué orden leer

### Integración y despliegue continuos

El artefacto central de este ejemplo es la definición del _pipeline_
de integración y despliegue continuos:
[.gitlab-ci.yml](.gitlab-ci.yml). El archivo tiene contenido en
formato YAML acorde a la especificación de GitlabCI, con comentarios
que explican cada sección superficialmente y que además incluyen
referencias que pueden consultarse para obtener más detalles.

### 12 factores

Muchas de las buenas prácticas de desarrollo de Software
contemporáneas se derivan directamente de los [12
factores](https://12factor.net/es/), y este ejemplo aplica
directamente algunos de ellos.

El primer principio a considerar en este ejemplo es el tercero de la
lista: "Guardar la configuración en el entorno". El archivo de
configuración de Django
[cicd_example/settings.py](cicd_example/settings.py) ha sido ajustado
para que las variables críticas de configuración sean leídas desde el
entorno. Se pueden observar las modificaciones que hacen uso de
`os.getenv()` para asignar valores a variables de configuración de
Django puntuales.

El segundo principio que este ejemplo resalta es el sexto de la lista:
"Ejecutar la aplicación como uno o más procesos sin estado". Esto
puntualmente se refiere al uso de la base de datos. Si bien SQLite es
adecuado como motor de bases de datos para aplicaciones locales o de
demostración, no es la opción correcta cuando se aspira a una
aplicación factorizada. La configuración Django de conexión a base de
datos fue modificada (en el archivo
[cicd_example/settings.py](cicd_example/settings.py)) para usar una
conexión a una instancia PostgreSQL. Esta sencilla consideración
permite a la aplicación **escalar horizontalmente** mediante
replicación (principio octavo: Concurrencia), y la acerca más al
principio noveno de la lista: Desechabilidad.

## Ejecución local

Se puede ejecutar localmente el ejemplo usando [docker
compose](https://docs.docker.com/compose/). Por ejemplo usando
[Compose v2](https://docs.docker.com/compose/cli-command/):

```bash
# Construir imagen docker
docker compose -f docker-compose-local.yml build
# Iniciar base de datos
docker compose -f docker-compose-local.yml up -d db
# Ejecutar tareas de administración Django
docker compose -f docker-compose-local.yml run --rm app python manage.py --help
# Iniciar aplicación, que quedará disponible en http://localhost:8080
docker compose -f docker-compose-local.yml up -d app
# Ejecutar linter
docker compose -f docker-compose-local.yml run --rm app flake8
# Ejecutar tests con reporte de cobertura
docker compose -f docker-compose-local.yml run --rm app \
       bash -c \
       'coverage run --rcfile=tox.ini manage.py test && coverage report --rcfile=tox.ini'
# Eliminar contenedores y volúmenes
docker compose -f docker-compose-local.yml down -v
```
