# Contribuir a Ejemplo CI/CD

¡Hola! Gracias por colaborar con este repositorio. Su aporte es muy
bienvenido :)

Para que su ayuda esté mejor canalizada, tenga presente lo siguiente:
el repositorio existe para ser usado como guía introductoria al amplio
mundo de la integración continua. El repositorio aspira por lo tanto a
ser:

- Una fuente de lectura antes que una aplicación útil.
- Accesible: debe invitar y ayudar a lectores sin experiencia en
  integración continua a familiarizarse con los conceptos.
- Relevante: debe ilustrar ideas vigentes por sobre conceptos en
  desuso.
- Simple: debe conformarse con la introducción de los lectores al
  mundo de la integración continua, y dirigirlos a otras fuentes de
  información para profundizar el conocimiento.

Como colaborador del repositorio, aspire a mejorar cualquiera de estos
aspectos ;)

## Cómo contribuir

La forma usual es mediante
[issues](https://gitlab.com/kai.klingenberg/cicd-example/-/issues),
que pueden ser anotadas con las etiquetas `solicitud`, `error` o
`conversación` de acuerdo al contenido.

Si desea colaborar con cambios en el repositorio deberá crear un
[merge
request](https://gitlab.com/kai.klingenberg/cicd-example/-/merge_requests),
que idealmente estará asociado a una _issue_ existente.

### Idioma

El repositorio aspira a contener explicaciones en el idioma castellano
neutro. Esto incluye archivos markdown (como este), y comentarios en
archivos de código fuente claves para la explicación de conceptos
relacionados a la integración continua (como el archivo
[.gitlab-ci.yml](.gitlab-ci.yml). El código fuente (nombres de
variables, etc.), además de comentarios de código fuente que no es
usado para ilustrar conceptos de CI/CD, deberá estar escrito en
inglés.

### Expectativas

Este proyecto es mantenido por personas con disponibilidad limitada,
de modo que es probable que nuevas _issues_ o _merge requests_ no
puedan ser atendidos tan rápidamente podría desearse. Tenga paciencia
:pray:
