#!/usr/bin/env bash

set -e

exists=$(
    python manage.py shell <<EOF
from django.contrib.auth import get_user_model
if get_user_model().objects.filter(is_superuser=True).exists():
    print("yes")
EOF
)

if test -z "${exists}"
then
    python manage.py createsuperuser \
           --noinput \
           --username admin \
           --email admin@example.com
else
    echo "Superuser already exists"
fi
