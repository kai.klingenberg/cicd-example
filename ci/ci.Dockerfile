FROM python:3.8.5-slim

RUN set -eux ; \
    apt-get update ; \
    apt-get install -y --no-install-recommends \
        netcat=1.10-41.1 \
        git=1:2.20.1-2+deb10u3 \
        wget=1.20.1-1.1 \
        libpq-dev=11.14-0+deb10u1 \
        build-essential=12.6 ; \
    rm -rf /var/lib/apt/lists/* ; \
    wget -O/usr/bin/jq https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64 ; \
    chmod +x /usr/bin/jq

WORKDIR /src
COPY requirements.txt ./
COPY ci ci
RUN pip install --no-cache-dir -r ci/requirements-ci.txt

COPY . .

ENTRYPOINT ["env"]
