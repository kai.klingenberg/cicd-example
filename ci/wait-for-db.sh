#!/usr/bin/env bash

while ! nc -z "${POSTGRES_HOSTNAME:-db}" "${POSTGRES_PORT:-5432}"
do
    echo "Waiting for PostgreSQL to be ready"
    sleep 1
done

exec "$@"
