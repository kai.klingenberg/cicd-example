# Utilidades para integración continua

Esta carpeta contiene artefactos usados en varias etapas del pipeline
de integración y despliegue continuos. En resumen:

- `ci.Dockerfile` es la definición de la imagen docker que se usa
  varios trabajos del pipeline, incluyendo el trabajo de despliegue.
- `requirements-ci.txt` es la declaración de dependencias python para
  la construcción de la imagen docker para uso en el pipeline.
- Archivos `*.env`, son archivos con la configuración de la aplicación
  en formato de variables de entorno.
- Archivos `*.sh`, son scripts _bash_ que simplifican algunas tareas
  puntuales.
- `proxy.conf`, es la configuración de un servicio
  [nginx](https://nginx.org/en/) usado en el despliegue como
  protección del servicio principal --la aplicación Django.
