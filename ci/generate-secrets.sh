#!/usr/bin/env bash

set -e

# This script generates random values for the application secrets, as
# needed.

randomtoken() {
    size="$1"
    python3 -c "import secrets; print(secrets.token_urlsafe(${size}))"
}

cat <<EOF
SECURITY_KEY=$(randomtoken 30)
DJANGO_SUPERUSER_PASSWORD=$(randomtoken 10)
POSTGRES_PASSWORD=$(randomtoken 20)
EOF
